package src.main.java.entity;
import java.util.Date;

public class Employe {
	
	private String nom;
	private boolean sexe;
	private int age;

	public Employe(String nomEmploye, boolean sexeEmploye, int ageE) {
		// TODO Auto-generated constructor stub
		this.nom = nomEmploye;
		this.sexe = sexeEmploye;
		this.age = ageE;
	}
	public String getNom(){
		return nom;
	}
	public boolean getSexe(){
		return sexe;
	}
	public int getAge(){
		return age;
	}
}
