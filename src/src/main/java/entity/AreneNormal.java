package src.main.java.entity;

import java.util.Date;
import java.util.List;

public class AreneNormal extends Enclos {

    public AreneNormal(String nomE, double superficieE, int maxE, String propreteE, int nbr_animauxE,
                       List<Pokemon> listPokemonE, Date timeValueE, String typeAreneE) {
        super();
        this.nom = nomE;
        this.superficie = superficieE;
        this.max = maxE;
        this.proprete = propreteE;
        this.nbrAnimaux = nbr_animauxE;
        this.listPokemon = listPokemonE;
        this.timeValue = timeValueE;
        this.typeArene = typeAreneE;
    }
}
