package src.main.java.entity;

import java.util.Date;
import java.util.List;

abstract public class Enclos {

    protected String nom;
    protected double superficie;
    protected int max;
    protected String proprete;
    protected int nbrAnimaux;
    protected List<Pokemon> listPokemon;
    protected Date timeValue;
    protected String typeArene;

    public Enclos() {

    }

    public Enclos(String nomE, double superficieE, int maxE, String propreteE, int nbr_animauxE,
                  List<Pokemon> listPokemonE, Date timeValueE, String typeAreneE) {
        this.nom = nomE;
        this.superficie = superficieE;
        this.max = maxE;
        this.proprete = propreteE;
        this.nbrAnimaux = nbr_animauxE;
        this.listPokemon = listPokemonE;
        this.timeValue = timeValueE;
        this.typeArene = typeAreneE;
    }

    public String getNom() {
        return nom;
    }

    public double getSuperficie() {
        return superficie;
    }

    public int getMax() {
        return max;
    }

    public String getProprete() {
        return proprete;
    }

    public int getNbrAnimaux() {
        return nbrAnimaux;
    }

    public List<Pokemon> getListPokemon() {
        return listPokemon;
    }

    public Date getTimeValue() {
        return timeValue;
    }

    public void setTimeValue(Date timeValueS) {
        timeValue = timeValueS;
    }

    public void setProprete(String etat) {
        proprete = etat;
    }
    public int getLastIdPokemon(){
        int cont = -1;
        for (Pokemon pokemon : listPokemon){
            cont++;
        }
        return cont;
    }
}