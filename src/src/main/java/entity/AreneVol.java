package src.main.java.entity;

import java.util.Date;
import java.util.List;

public class AreneVol extends Enclos {
    private double hauteur;

    public AreneVol(String nomE, double superficieE, int maxE, String propreteE, int nbr_animauxE, double hauteurV,
                    List<Pokemon> listPokemonV, Date timeE, String typeAreneE) {
        this.nom = nomE;
        this.superficie = superficieE;
        this.max = maxE;
        this.proprete = propreteE;
        this.nbrAnimaux = nbr_animauxE;
        this.hauteur = hauteurV;
        this.listPokemon = listPokemonV;
        this.timeValue = timeE;
        this.typeArene = typeAreneE;
    }

    public double getHauteur() {
        return hauteur;
    }

    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }
}
