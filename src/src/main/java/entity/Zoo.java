package src.main.java.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Zoo {

    private String nom;

    public Zoo(String nom_zoo){
        this.nom = nom_zoo;
    }

    private static Scanner scanner = new Scanner(System.in);
    private static List<Enclos> listeEnclos = new ArrayList<Enclos>();
    private static DateFormat dateFormat = new SimpleDateFormat("HH");

    public static void initListEnclos(List<Enclos> listeEnclosZoo){
        listeEnclos = listeEnclosZoo;
        System.out.println("Bienvenue ! Je suis le professeur Chen, beaucoup de personne pense que n'importe qui peut �tre dresseur Pokemon mais c'est faux ! \n" + 
        		"Pour �tre un dresseur Pokemon reconnu par l'�tat il faut passer par plusieurs �tapes et c'est maintenant � votre tour de relever ce challenge.\n" + 
        		"Vous �tes � pr�sent Ranger Pok�mon par interim, faites un tour dans les ar�nes, nourrissez les Pokemon, soignez-les, nettoyez leurs enclos et qui sait vous aurez aussi le droit d'avoir votre cr�ature si vous �tes assez dou�.\n" + 
        		"Enfin dans une nouvelle aventure peut-�tre haha, bon je vais retourner travailler voici les cl�s des ar�nes et � bient�t !");
    }

    public static void chooseEnclos() {
        System.out.println("Liste des ar�nes : ");
        System.out.println("Dans quelle ar�ne voulez-vous vous rendre ? Tapez le le chiffre correspondant � votre choix ar�ne");
        int i = 1;
        for (Enclos enclo : listeEnclos) {
            System.out.println(i + ": " + enclo.getNom());
            i++;
        }
        String enclosSelect = scanner.next();
        if (!enclosSelect.equals("1") && !enclosSelect.equals("2")&& !enclosSelect.equals("3")&& !enclosSelect.equals("4")&& !enclosSelect.equals("5")&& !enclosSelect.equals("6")&& !enclosSelect.equals("7")&& !enclosSelect.equals("8")&& !enclosSelect.equals("9")) {
        	System.out.println("Mauvaise saisie");
        	enclosSelect = null;
        	chooseEnclos();
        }
        
        
        if (enclosSelect.equals("1")) {
            gestionEnclos(listeEnclos.get(0));
        } else if (enclosSelect.equals("2")) {
            gestionEnclos(listeEnclos.get(1));
        } else if (enclosSelect.equals("3")) {
            gestionEnclos(listeEnclos.get(2));
        } else if (enclosSelect.equals("4")) {
            gestionEnclos(listeEnclos.get(3));
        } else if (enclosSelect.equals("5")) {
            gestionEnclos(listeEnclos.get(4));
        } else if (enclosSelect.equals("6")) {
            gestionEnclos(listeEnclos.get(5));
        } else if (enclosSelect.equals("7")) {
            gestionEnclos(listeEnclos.get(6));
        } else if (enclosSelect.equals("8")) {
            gestionEnclos(listeEnclos.get(7));
        } else if (enclosSelect.equals("9")) {
            gestionEnclos(listeEnclos.get(8));
        }
    }

    public static void gestionEnclos(Enclos enclosSelected) {
        String rep;
        Date dateE = new Date();
        if (enclosSelected.getTimeValue().getTime() + 20000 < dateE.getTime()) {
        	enclosSelected.setTimeValue(dateE);
        	enclosSelected.setProprete("sale");
        }
        for (Pokemon pokemon : enclosSelected.getListPokemon()) {
        	if (pokemon.getTimeLastFeed().getTime() + 5000 < dateE.getTime()) {
	           pokemon.setTimeLastFeed(dateE);
	           pokemon.setFaim(1);
            }
        	if (pokemon.getTimeLastHeal().getTime() + 5000 < dateE.getTime()) {
 	           pokemon.setTimeLastHeal(dateE);
 	           pokemon.setSante(1);
             }
        	if (pokemon.getTimeSommeil().getTime() + 5000 < dateE.getTime()) {
  	           pokemon.setTimeSommeil(dateE);
               pokemon.setSommeil(true);
              }
        }

        System.out.println("Quelle action voulez-vous effectuer ? Tapez le le chiffre correspondant � votre action");
        System.out.println("1. Voir les informations de l'ar�ne");
        System.out.println("2. D�placer un Pokemon");
        System.out.println("3. Nourrir les Pokemon");
        System.out.println("4. Soigner les Pokemon.");
        System.out.println("5. Nettoyer l'enclos.");
        System.out.println("6. R�veiller les Pokemon.");
        System.out.println("7. Retour � la selection de l'ar�ne");
        rep = scanner.next();
        if (!rep.equals("1") && !rep.equals("2")&& !rep.equals("3")&& !rep.equals("4")&& !rep.equals("5")&& !rep.equals("6")&& !rep.equals("7")) {
        	System.out.println("Mauvaise saisie");
        	rep = null;
        	gestionEnclos(enclosSelected);
        }
        if (rep.equals("1")) {
            getInfoEnclos(enclosSelected);
        } else if (rep.equals("2")) {
            movePokemonEnclos(enclosSelected);
        } else if (rep.equals("5")) {
            netEnclos(enclosSelected);
        } else if (rep.equals("3")) {
            feedPokemon(enclosSelected);
        }else if (rep.equals("4")) {
            healPokemon(enclosSelected);
        } else if (rep.equals("6")) {
            reveillePokemon(enclosSelected);
        }else if (rep.equals("7")) {
            chooseEnclos();
        }

    }

    public static void reveillePokemon(Enclos enclos) {
        Date dateE = new Date();
        for (Pokemon pokemon : enclos.getListPokemon()) {
        	pokemon.setTimeSommeil(dateE);
            pokemon.setSommeil(false);
            System.out.println("Le Pokemon : " + pokemon.getNom() + " a été reveill� | Status" + pokemon.getSommeil());
        }
        gestionEnclos(enclos);
    }
    public static void feedPokemon(Enclos enclos) {
        Date dateP = new Date();
        for (Pokemon pokemon : enclos.getListPokemon()) {
        	if(pokemon.getSommeil() == false) {
        		if (pokemon.getFaim() == 1) {
                    System.out.println("Le POKEMON : " + pokemon.getNom() + " a été nourri");
                    pokemon.setTimeLastFeed(dateP);
                    pokemon.setFaim(0);
                    System.out.println("Nom : " + pokemon.getNom() + " FAIM : OK ");
                }
        	}else {
                System.out.println("Nom : " + pokemon.getNom() + " Pokemon endormi,veuillez le reveiller pour le nourrir");
        	}
            
        }

        gestionEnclos(enclos);
    }
    public static void healPokemon(Enclos enclos) {
        Date dateP = new Date();
        for (Pokemon pokemon : enclos.getListPokemon()) {
        	if(pokemon.getSommeil() == false) {
        		if (pokemon.getSante() == 1) {
                    System.out.println("Le POKEMON : " + pokemon.getNom() + " a été soign�");
                    pokemon.setTimeLastHeal(dateP);
                    pokemon.setSante(0);
                    System.out.println("Nom : " + pokemon.getNom() + " HEAL : OK ");

                }

        	}else {
                System.out.println("Nom : " + pokemon.getNom() + " Pokemon endormi,veuillez le reveiller pour le heal");
        	}
        }
      
        gestionEnclos(enclos);
    }

    public static void netEnclos(Enclos enclos) {
        enclos.setProprete("Propre");
        enclos.setTimeValue(new Date());
        System.out.println("Votre enclos est maitenant propre.");
        gestionEnclos(enclos);
    }

    public static void movePokemonEnclos(Enclos enclosSelected) {
        int i = 0;
        int repPokemon;
        String repPokemonType;
        int repEnclos;
        List<Pokemon> choose = new ArrayList<Pokemon>();

        if (enclosSelected.getNom().equals("PC Prof. Chen")) {
            System.out.println("Renseignez le type de pokemon voulu {eau;normal;vol;leg}");
            System.out.println("Faites attention � vos saisie respecter bien ce qui est demand�e!");
            repPokemonType = scanner.next();

            for (Pokemon pokemon : enclosSelected.getListPokemon()) {
                if (pokemon.getType().equals(repPokemonType)) {
                    choose.add(pokemon);
                }
            }
            if (choose.isEmpty()) {

                for (Pokemon pokemon : choose) {
                    System.out.println("Renseignez le N° du pokemon");
                    if (pokemon.getType().equals(repPokemonType)) {
                        System.out.println("N°" + pokemon.getId() + " : " + pokemon.getNom());
                    }
                    repPokemon = scanner.nextInt();
                    i = 1;
                    System.out.println("Selectionnez votre Enclos s'il s'agit d'un enclos normal sinon le pokemon va �tre d�plac� dans le PC");

                    for (Enclos enclos : listeEnclos) {
                        System.out.println(i + ": " + enclos.getNom() + " | Nombre de pokemon dans l'arène :" + enclos.nbrAnimaux + " | Nombre max de pokemon :" + enclos.getMax());
                        i++;
                    }
                    repEnclos = scanner.nextInt();
                    if (repEnclos == 1) {
                        if (listeEnclos.get(0).getMax() != listeEnclos.get(0).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(0).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 2) {
                        if (listeEnclos.get(1).getMax() != listeEnclos.get(1).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "aqua") {
                                listeEnclos.get(1).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type eau sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 3) {
                        if (listeEnclos.get(2).getMax() != listeEnclos.get(2).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(2).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 4) {
                        if (listeEnclos.get(3).getMax() != listeEnclos.get(3).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(3).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 5) {
                        if (listeEnclos.get(4).getMax() != listeEnclos.get(4).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(4).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 6) {
                        if (listeEnclos.get(5).getMax() != listeEnclos.get(5).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(5).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 7) {
                        if (listeEnclos.get(6).getMax() != listeEnclos.get(6).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "vol") {
                                listeEnclos.get(0).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type vol sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 8) {
                        if (listeEnclos.get(7).getMax() != listeEnclos.get(7).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "leg") {
                                listeEnclos.get(7).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type legendaire sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 9) {
                        if (listeEnclos.get(8).getMax() != listeEnclos.get(8).getListPokemon().size()) {
                            listeEnclos.get(8).getListPokemon().add(enclosSelected.getListPokemon().get(repPokemon));
                            enclosSelected.getListPokemon().remove(repPokemon);
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    }

                }
            } else {
                System.out.println("Aucun Pokemon n'est présent dans l'Enclos");
                movePokemonEnclos(enclosSelected);
            }

        } else {

            if (!enclosSelected.getListPokemon().isEmpty()) {

                for (Pokemon pokemon : enclosSelected.getListPokemon()) {
                    System.out.println("N°" + i + " : " + pokemon.getNom());
                    i++;
                }
                repPokemon = scanner.nextInt();
                i = 1;
                System.out.println("Selectionnez votre Enclos");

                if (enclosSelected.getNom() != "Mauville" && enclosSelected.getNom() != "Azuria"
                        && enclosSelected.getNom() != "Marseille") {

                    for (Enclos enclos : listeEnclos) {
                        System.out.println(i + ": " + enclos.getNom() + " | Nombre de pokemon dans l'arène :" + enclos.nbrAnimaux + " | Nombre max de pokemon :" + enclos.getMax());
                        i++;
                    }
                        repEnclos = scanner.nextInt();
                    if (repEnclos == 1) {
                        if (listeEnclos.get(0).getMax() != listeEnclos.get(0).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(0).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 2) {
                        if (listeEnclos.get(1).getMax() != listeEnclos.get(1).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "aqua") {
                                listeEnclos.get(1).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type eau sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 3) {
                        if (listeEnclos.get(2).getMax() != listeEnclos.get(2).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(2).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 4) {
                        if (listeEnclos.get(3).getMax() != listeEnclos.get(3).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(3).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 5) {
                        if (listeEnclos.get(4).getMax() != listeEnclos.get(4).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(4).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 6) {
                        if (listeEnclos.get(5).getMax() != listeEnclos.get(5).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "normal") {
                                listeEnclos.get(5).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type normal sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 7) {
                        if (listeEnclos.get(6).getMax() != listeEnclos.get(6).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "vol") {
                                listeEnclos.get(0).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type vol sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 8) {
                        if (listeEnclos.get(7).getMax() != listeEnclos.get(7).getListPokemon().size()) {
                            if (enclosSelected.getListPokemon().get(repPokemon).getType() == "leg") {
                                listeEnclos.get(7).getListPokemon()
                                        .add(enclosSelected.getListPokemon().get(repPokemon));
                                enclosSelected.getListPokemon().remove(repPokemon);
                            } else {
                                System.out.println(
                                        "Nous somme dsl seul les pokemons de type legendaire sont pris en charge ici. ");
                                movePokemonEnclos(enclosSelected);
                            }
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    } else if (repEnclos == 9) {
                        if (listeEnclos.get(8).getMax() != listeEnclos.get(8).getListPokemon().size()) {
                            listeEnclos.get(8).getListPokemon().add(enclosSelected.getListPokemon().get(repPokemon));
                            enclosSelected.getListPokemon().remove(repPokemon);
                        } else {
                            System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                            movePokemonEnclos(enclosSelected);
                        }
                    }
                } else {
                    if (listeEnclos.get(8).getMax() != listeEnclos.get(8).getListPokemon().size()) {
                        listeEnclos.get(8).getListPokemon().add(enclosSelected.getListPokemon().get(repPokemon));
                        enclosSelected.getListPokemon().remove(repPokemon);
                        System.out.println("Votre pokemon a été déplacé dans le PC");
                    } else {
                        System.out.println("Nous somme dsl l'Enclos est déjà plein ! ");
                        movePokemonEnclos(enclosSelected);
                    }
                }
            } else {
                System.out.println("Aucun Pokemon n'est présent dans l'Enclos");
                movePokemonEnclos(enclosSelected);
            }
        }

        gestionEnclos(enclosSelected);

    }

    public static void getInfoEnclos(Enclos enclos) {
        Date dateE = new Date();
        boolean ifRand = false;
        
        for (Pokemon pokemon : enclos.getListPokemon()){
            if(pokemon.isSexe()){
                if (pokemon.getType().equals("leg")) {
                    ifRand  = ((Math.random() * 1) != 0);
                    if (ifRand){
                        listeEnclos.get(8).getListPokemon().add(new Pokemon(enclos.getLastIdPokemon()+1, pokemon.getNom(),
                                ((int) (Math.random() * 1) != 0), (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                                dateFormat.format(dateE), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                                (int) (Math.random() * 1), false, "leg", new Date(),pokemon.getCri()));
                    }

                }else if(pokemon.getType().equals("eau")) {
                    ifRand  = ((Math.random() * 1) != 0);
                    if (ifRand) {
                        listeEnclos.get(8).getListPokemon().add(new Pokemon(enclos.getLastIdPokemon()+1, "oeuf de " + pokemon.getNom(),
                                ((int) (Math.random() * 1) != 0), (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                                dateFormat.format(dateE), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                                (int) (Math.random() * 1), true, "eau", new Date(), 8,pokemon.getCri()));
                    }

                }else if(pokemon.getType().equals("vol")) {
                    ifRand  = ((Math.random() * 1) != 0);
                    if (ifRand) {
                        listeEnclos.get(8).getListPokemon().add(new Pokemon(enclos.getLastIdPokemon()+1, "oeuf de " + pokemon.getNom(),
                                ((int) (Math.random() * 1) != 0), (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                                dateFormat.format(dateE), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                                (int) (Math.random() * 1), true, "eau", new Date(), 8,pokemon.getCri()));
                    }
                }else if(pokemon.getType().equals("normal")) {
                    ifRand  = ((Math.random() * 1) != 0);
                    if (ifRand) {
                        listeEnclos.get(8).getListPokemon().add(new Pokemon(enclos.getLastIdPokemon()+1, "oeuf de " + pokemon.getNom(),
                                ((int) (Math.random() * 1) != 0), (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                                dateFormat.format(dateE), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                                (int) (Math.random() * 1), true, "eau", new Date(), 8,pokemon.getCri()));
                    }
                }
            }
            if (pokemon.getDuree_incubation() != -1){
                if (pokemon.getTimeLeftIncubation() != -1){
                    pokemon.setTimeLeftIncubation(pokemon.getTimeLeftIncubation() - 1);
                }
                if(pokemon.getTimeLeftIncubation() == 0){
                    pokemon.setNom(pokemon.getNom().substring(8,pokemon.getNom().length()));
                }
            }

        }
        if (enclos.typeArene == "normal") {
        	 System.out.println("Nom de l'Enclos : " + enclos.getNom());
             System.out.println("Superficie : " + enclos.getSuperficie());
             System.out.println("Capacitée max : " + enclos.getMax());
             System.out.println("Proprete : " + enclos.getProprete());
        }else if(enclos.typeArene == "vol"){
            AreneVol enclosVol = (AreneVol) enclos;
       	 	System.out.println("Nom de l'Enclos : " + enclosVol.getNom());
            System.out.println("Superficie : " + enclosVol.getSuperficie());
            System.out.println("Capacitée max : " + enclosVol.getMax());
            System.out.println("Proprete : " + enclosVol.getProprete());
            System.out.println("Hauteur : " + enclosVol.getHauteur());
        }else if(enclos.typeArene == "aqua"){
        	AreneAqua enclosAqua = (AreneAqua) enclos;
       	 	System.out.println("Nom de l'Enclos : " + enclosAqua.getNom());
            System.out.println("Superficie : " + enclosAqua.getSuperficie());
            System.out.println("Capacitée max : " + enclosAqua.getMax());
            System.out.println("Proprete : " + enclosAqua.getProprete());
            System.out.println("Profondeur : " + enclosAqua.getProfondeur());
            System.out.println("Salinite : " + enclosAqua.getSalinite());
        }
       
        System.out.println("Liste des pokemons present : ");
        for (Pokemon pokemon : enclos.getListPokemon()) {
            System.out.println("Nom : " + pokemon.getNom() + " | type : " + pokemon.getType()+ " | poid : " + pokemon.getPoids()+ " Kg | Sommeil : " + (pokemon.getSommeil() == true ? "dort" : "debout") + " | sant� : " +(pokemon.getSante() == 0 ? "Ok" : "Ko") + " | cri : " +pokemon.getCri() + " | faim : " +(pokemon.getFaim() == 0 ? "Le pokemon est rassasi�" : "Le pokemon a besoin d'�tre nourri"));
        }
        gestionEnclos(enclos);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
