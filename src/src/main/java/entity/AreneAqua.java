package src.main.java.entity;

import java.util.Date;
import java.util.List;

public class AreneAqua extends Enclos {

    private double profondeur;
    private int salinite;

    public AreneAqua(String nomE, double superficieE, int maxE, String propreteE, int nbr_animauxE, double profondeurA,
                     int saliniteA, List<Pokemon> listPokemonE, Date timeE,  String typeAreneE) {
        this.nom = nomE;
        this.superficie = superficieE;
        this.max = maxE;
        this.proprete = propreteE;
        this.nbrAnimaux = nbr_animauxE;
        this.profondeur = profondeurA;
        this.salinite = saliniteA;
        this.listPokemon = listPokemonE;
        this.timeValue = timeE;
        this.typeArene = typeAreneE;
    }

    public double getProfondeur() {
        return profondeur;
    }

    public void setProfondeur(double profondeur) {
        this.profondeur = profondeur;
    }

    public int getSalinite() {
        return salinite;
    }

    public void setSalinite(int salinite) {
        this.salinite = salinite;
    }
}
