package src.main.java.entity;

import java.util.HashMap;
import java.util.Hashtable;

public class PokemonNameCry {
    private String name;
    private String cry;

    public PokemonNameCry(String nameC, String cryC){
        this.cry = cryC;
        this.name = nameC;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setCry(String cry) {
        this.cry = cry;
    }

    public String getCry() {
        return cry;
    }
}
