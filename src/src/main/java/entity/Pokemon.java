package src.main.java.entity;

import java.util.Date;

public class Pokemon {

    private int id;
    private String nom;
    private boolean sexe;
    private double poids;
    private double taille;
    private String birthday;
    private boolean sommeil;
    private int faim;
    private int sante;
    private boolean ovipare;
    private int duree_incubation;
    private String type;
    private int timeLeftIncubation;
    private Date timeLastFeed;
    private Date timeLastHeal;
    private Date timeSommeil;
    private String cri;

    public Pokemon(int idA, String nomAnimal, boolean sexeAnimal, double poidsAnimal, double tailleAnimal,
                   String birthdayAnimal, boolean sommeilAnimal, int faimAnimal, int santeAnimal, boolean ovipareAnimal,
                   String typeA, Date timeLastFeedAnimal, String criAnimal) {
        // TODO Auto-generated constructor stub
        this.id = idA;
        this.nom = nomAnimal;
        this.sexe = sexeAnimal;
        this.poids = poidsAnimal;
        this.taille = tailleAnimal;
        this.birthday = birthdayAnimal;
        this.sommeil = sommeilAnimal;
        this.faim = faimAnimal;
        this.sante = santeAnimal;
        this.ovipare = ovipareAnimal;
        this.type = typeA;
        this.timeLastFeed = timeLastFeedAnimal;
        this.timeLastHeal = timeLastFeedAnimal;
        this.timeSommeil = timeLastFeedAnimal;
        this.duree_incubation = -1;
        this.timeLeftIncubation = -1;
        this.setCri(criAnimal);
    }
    public Pokemon(int idA, String nomAnimal, boolean sexeAnimal, double poidsAnimal, double tailleAnimal,
                   String birthdayAnimal, boolean sommeilAnimal, int faimAnimal, int santeAnimal, boolean ovipareAnimal,
                   String typeA, Date timeLastFeedAnimal, int duree_incubation,String criAnimal) {
        // TODO Auto-generated constructor stub
        this.id = idA;
        this.nom = nomAnimal;
        this.sexe = sexeAnimal;
        this.poids = poidsAnimal;
        this.taille = tailleAnimal;
        this.birthday = birthdayAnimal;
        this.sommeil = sommeilAnimal;
        this.faim = faimAnimal;
        this.sante = santeAnimal;
        this.ovipare = ovipareAnimal;
        this.type = typeA;
        this.timeLastFeed = timeLastFeedAnimal;
        this.timeLastHeal = timeLastFeedAnimal;
        this.timeSommeil = timeLastFeedAnimal;
        this.duree_incubation = duree_incubation;
        this.timeLeftIncubation = duree_incubation;
        this.setCri(criAnimal);
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getFaim() {
        return faim;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public Date getTimeLastFeed() {
        return timeLastFeed;
    }

    public Date getTimeLastHeal() {
        return timeLastHeal;
    }

    public boolean getSommeil() {
        return sommeil;
    }

    public void setTimeLastFeed(Date date) {
        timeLastFeed = date;
    }

    public void setTimeLastHeal(Date date) {
        timeLastHeal = date;
    }

    public void setSommeil(boolean etat) {
        sommeil = etat;
    }

    public void setFaim(int faim) {
        this.faim = faim;
    }

    public boolean isSexe() {
        return sexe;
    }

    public void setSexe(boolean sexe) {
        this.sexe = sexe;
    }

    public double getPoids() {
        return poids;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }

    public double getTaille() {
        return taille;
    }

    public void setTaille(double taille) {
        this.taille = taille;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSante() {
        return sante;
    }

    public void setSante(int sante) {
        this.sante = sante;
    }

    public boolean isOvipare() {
        return ovipare;
    }

    public void setOvipare(boolean ovipare) {
        this.ovipare = ovipare;
    }

    public int getDuree_incubation() {
        return duree_incubation;
    }

    public void setDuree_incubation(int duree_incubation) {
        this.duree_incubation = duree_incubation;
    }

    public Date getTimeSommeil() {
        return timeSommeil;
    }

    public void setTimeSommeil(Date timeSommeil) {
        this.timeSommeil = timeSommeil;
    }

    public int getTimeLeftIncubation() {
        return timeLeftIncubation;
    }

    public void setTimeLeftIncubation(int timeLeftIncubation) {
        this.timeLeftIncubation = timeLeftIncubation;
    }
	public String getCri() {
		return cri;
	}
	public void setCri(String cri) {
		this.cri = cri;
	}
}
