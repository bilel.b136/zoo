package src.main.java.app;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

import src.main.java.entity.*;
public class start {
    private static Scanner scanner = new Scanner(System.in);
    private static List<Enclos> listeEnclos = new ArrayList<Enclos>();
    private static final AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) {
        new Zoo("Kanto Park");
        String nomE;
        String sexeE;
        Integer age = 00;
        System.out.println("Indiquez votre nom :");
        nomE = scanner.next();  
        System.out.println("Etes vous un gar�on ou une fille ?  Tapez 1 pour gar�on et 2 pour une fille");
        sexeE = scanner.next();
        while(!sexeE.equals("1") && !sexeE.equals("2")) {
            System.out.println("Mauvaise saisie");
            System.out.println("Etes vous un gar�on ou une fille ?  Tapez 1 pour gar�on et 2 pour une fille");
            sexeE = scanner.next();
        }
        System.out.println("Indiquez votre �ge : ");
        try{
            age = scanner.nextInt();
        }catch (Exception e){
            System.out.println("Mauvaise saisie");
            main(args);
        }
        Employe employe = new Employe(nomE, (sexeE == "1") ? true : false, age);

        System.out.println("welcome " + employe.getNom());
        List<Pokemon> listPokemon = new ArrayList<Pokemon>();
        List<Pokemon> listPokemonAqua = new ArrayList<Pokemon>();
        List<Pokemon> listPokemonVol = new ArrayList<Pokemon>();
        List<Pokemon> listPokemonLeg = new ArrayList<Pokemon>();
        List<Pokemon> listPokemonPc = new ArrayList<Pokemon>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        int rand;
        int randSpec;
        Date date = new Date();
        for (int z = 0; z < 5; z++) {
            rand = (int) (Math.random() * 11);
            randSpec = (int) (Math.random() * 5);
            listPokemon.add(new Pokemon(count.incrementAndGet(), getNamePokemon().get(rand).getName(),
            		Math.random() < 0.5, (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                    dateFormat.format(date), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                    (int) (Math.random() * 1), true, "normal", new Date(), getNamePokemon().get(rand).getCry()));
            listPokemonAqua.add(new Pokemon(count.incrementAndGet(), getNamePokemonAqua().get(randSpec).getName(),
            		Math.random() < 0.5, (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                    dateFormat.format(date), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                    (int) (Math.random() * 1), true, "eau", new Date(), getNamePokemon().get(rand).getCry()));
            listPokemonVol.add(new Pokemon(count.incrementAndGet(), getNamePokemonVol().get(randSpec).getName(),
            		Math.random() < 0.5, (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                    dateFormat.format(date), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                    (int) (Math.random() * 1), true, "vol", new Date(), getNamePokemon().get(rand).getCry()));
            listPokemonLeg.add(new Pokemon(count.incrementAndGet(), getNamePokemonLeg().get(randSpec).getName(),
            		Math.random() < 0.5, (Math.random() * 200) + 20, (Math.random() * 300) + 20,
                    dateFormat.format(date), ((int) (Math.random() * 1) != 0), (int) (Math.random() * 1),
                    (int) (Math.random() * 1), false, "leg", new Date(), getNamePokemon().get(rand).getCry()));
        }
        for (int i = 0; i < 9; i++) {
            if (getNameEnclos().get(i) == "Azuria") {
                listeEnclos.add(new AreneAqua(getNameEnclos().get(i), (Math.random() * 60) + 20, 5, "bon", listPokemonAqua.size(),
                        (Math.random() * 20) + 20, (int) (Math.random() * 2) + 2, listPokemonAqua, new Date(), "aqua"));
            } else if (getNameEnclos().get(i) == "Mauville") {
                listeEnclos.add(new AreneVol(getNameEnclos().get(i), (int) (Math.random() * 60) + 20, 5, "bon", listPokemonVol.size(),
                        (Math.random() * 20) + 20, listPokemonVol, new Date(), "vol"));
            } else if (getNameEnclos().get(i) == "Marseille") {
                listeEnclos.add(new AreneNormal(getNameEnclos().get(i), (int) (Math.random() * 60) + 20, 5, "bon", listPokemonLeg.size(),
                        listPokemonLeg, new Date(), "normal"));
            } else if (getNameEnclos().get(i) == "PC Prof. Chen") {
                listeEnclos.add(new AreneNormal(getNameEnclos().get(i), (int) (Math.random() * 60) + 20, 1000, "bon", listPokemon.size(),
                        listPokemonPc, new Date(), "normal"));
            } else {
                listeEnclos.add(new AreneNormal(getNameEnclos().get(i), (int) (Math.random() * 60) + 20, 5, "bon", listPokemon.size(),
                        listPokemon, new Date(), "normal"));
            }
        }
        Zoo.initListEnclos(listeEnclos);
        Zoo.chooseEnclos();
    }


    private static List<String> getNameEnclos() {
        List<String> listNomEnclos = new ArrayList<String>();
        listNomEnclos.add("Argenta");
        listNomEnclos.add("Azuria");
        listNomEnclos.add("Carmin sur Mer");
        listNomEnclos.add("Parmanie");
        listNomEnclos.add("Safrania");
        listNomEnclos.add("Celadopole");
        listNomEnclos.add("Mauville");
        listNomEnclos.add("Marseille");
        listNomEnclos.add("PC Prof. Chen");
        return listNomEnclos;
    }

    private static List<PokemonNameCry> getNamePokemon() {
        List<PokemonNameCry> listNomPokemon = new ArrayList<PokemonNameCry>();
        listNomPokemon.add(new PokemonNameCry("Dracolosse", "ooooooooo"));
        listNomPokemon.add(new PokemonNameCry("Pikachu", "Pika-pika !"));
        listNomPokemon.add(new PokemonNameCry("Tauros", "Hmrrrrr"));
        listNomPokemon.add(new PokemonNameCry("Alakazam", "A-La-Ka-Zam"));
        listNomPokemon.add(new PokemonNameCry("Arcanin", "Ouaaaa"));
        listNomPokemon.add(new PokemonNameCry("Magmar", "Kwoiiiiin"));
        listNomPokemon.add(new PokemonNameCry("Insecateur", "Insecateeeeur"));
        listNomPokemon.add(new PokemonNameCry("Kangourex", "Kangouuuuuuuu"));
        listNomPokemon.add(new PokemonNameCry("Ectoplasma", "Ish-Ish"));
        listNomPokemon.add(new PokemonNameCry("Dracaufeu", "Braaaaa"));
        listNomPokemon.add(new PokemonNameCry("Voltali", "Miaouuuuu"));
        listNomPokemon.add(new PokemonNameCry("Rhinoferos", "RRRRhg"));
        return listNomPokemon;
    }

    private static List<PokemonNameCry> getNamePokemonVol() {
        List<PokemonNameCry> listNomPokemon = new ArrayList<PokemonNameCry>();
        listNomPokemon.add(new PokemonNameCry("Roucarnage", "Skuuuuuurrr"));
        listNomPokemon.add(new PokemonNameCry("Rapasdepic", "Raaaapasdepiiic"));
        listNomPokemon.add(new PokemonNameCry("Nosferalto", "Titititiiiii"));
        listNomPokemon.add(new PokemonNameCry("Ptera", "Raaaa"));
        listNomPokemon.add(new PokemonNameCry("Papilusion", "Miii"));
        listNomPokemon.add(new PokemonNameCry("Dardargnan", "Bzzzzzzz"));
        return listNomPokemon;
    }

    private static List<PokemonNameCry> getNamePokemonAqua() {
        List<PokemonNameCry> listNomPokemon = new ArrayList<PokemonNameCry>();
        listNomPokemon.add(new PokemonNameCry("Tortank", "Toooortank"));
        listNomPokemon.add(new PokemonNameCry("Leviator", "GRAAAAAA"));
        listNomPokemon.add(new PokemonNameCry("Akwakwak", "Akwakwakwakwaaaak"));
        listNomPokemon.add(new PokemonNameCry("Tartard", "Taltalos"));
        listNomPokemon.add(new PokemonNameCry("Lokhlass", "Looooooooo"));
        listNomPokemon.add(new PokemonNameCry("Wailord", "Fulnuke"));
        return listNomPokemon;
    }

    private static List<PokemonNameCry> getNamePokemonLeg() {
        List<PokemonNameCry> listNomPokemon = new ArrayList<PokemonNameCry>();
        listNomPokemon.add(new PokemonNameCry("Artikodin", "Kodoooo"));
        listNomPokemon.add(new PokemonNameCry("Electhor", "Kruiii"));
        listNomPokemon.add(new PokemonNameCry("Entei", "Wouuuu"));
        listNomPokemon.add(new PokemonNameCry("Raikou", "Grrrrrrrrrrrrrr"));
        listNomPokemon.add(new PokemonNameCry("Lugia", "Loulou"));
        listNomPokemon.add(new PokemonNameCry("Rayquaza", " . . . "));
        return listNomPokemon;
    }

}
